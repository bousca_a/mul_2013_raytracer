/*
** texture.h for rt in /home/camill_n/mul_2013_raytracer
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Jun  8 02:56:34 2014 camill_n
** Last update Sun Jun  8 23:08:30 2014 Maxime Boguta
*/

#ifndef TEXTURE_H_
# define TEXTURE_H_

typedef struct s_all		t_all;
typedef struct s_obj		t_obj;
typedef struct s_color		t_color;
typedef struct s_texture	t_texture;
typedef struct s_hit		t_hit;
typedef struct s_img		t_img;

int		load_texture(t_all *all, char *path, t_img *img);
void		init_texture(t_all *all);
t_texture	*get_texture(t_all *all, char *name);
void		set_color_texture(t_all *all, t_hit *hit, int *cur_px_calc);

#endif
