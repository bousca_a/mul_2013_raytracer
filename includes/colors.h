/*
** colors.h for rt in /home/boguta_m/rendu/mul_2013_raytracer/includes
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Tue Jun  3 15:22:25 2014 Maxime Boguta
** Last update Thu Jun  5 13:54:29 2014 Maxime Boguta
*/

#ifndef _COLORS_H_
# define _COLORS_H_

# include "struct.h"

void	_simple_color(t_tmp *tmp, t_color *col, double fac);
void	_mix_colors(t_color *from, t_color *to, int stren, t_tmp *tmp);
void	_color_factor(t_tmp *tmp, t_color *col, t_light *lig, double fac);

#endif
