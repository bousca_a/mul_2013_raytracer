/*
** network.h for rt in /home/camill_n/mul_2013_raytracer
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Fri Jun  6 14:40:18 2014 camill_n
** Last update Sun Jun  8 01:46:26 2014 camill_n
*/

#ifndef NETWORK_H_
# define NETWORK_H_

# include <netdb.h>
# include <netinet/in.h>
# include <sys/socket.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <sys/un.h>

# define PAQUET_SIZE 10240
# define PORT 2500

typedef struct sockaddr_in	t_sock_in;
typedef struct s_all		t_all;

typedef struct		s_sock
{
  int			sock;
  t_sock_in		info;
}			t_sock;

typedef struct		s_host
{
  int			sock;
  long			addr_host;
  struct hostent	*host;
  t_sock_in		info;
  int			start_interval;
  int			size_interval;
  struct s_host		*next;
}			t_host;

typedef struct		s_cluster
{
  int			nb_h;
  int			nb_host;
  t_sock		sock_client;
  int			mode_host;
  int			nb_c;
  int			mode_client;
  int			client_connect;
  t_host		*host;
  t_sock		sock;
  char			*content;
}			t_cluster;

int	connect_server(t_all *all, t_host *host, char *srv);
t_host	*add_host(t_all *all, int start_interval, int size_interval, char *srv);
void	init_srv_socket(t_all *all);
void	wait_for_xml(t_all *all);
void	wait_for_client(t_all *all);
void	init_cluster(t_cluster *cluster);
void	active_server(t_all *all);
void	active_client(t_all *all);
void	send_xml(t_all *all, t_host *host, int nb_c);
void	init_cluster(t_cluster *cluster);
void	send_img(t_all *all);
void	get_result_from_host(t_all *all, int size_paq, int size_tot);
void	send_result_to_client(t_all *all, int size_paq, int size_tot);
void	send_my_xml(char *src, t_host *);
void	get_my_xml(t_all *all, char *src);

#endif
