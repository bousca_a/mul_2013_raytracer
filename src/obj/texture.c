/*
** texture.c for rt in /home/camill_n/mul_2013_raytracer
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Jun  8 02:52:13 2014 camill_n
** Last update Sun Jun  8 23:07:31 2014 Maxime Boguta
*/

#include "struct.h"

int	load_texture(t_all *all, char *path, t_img *img)
{
  int	ret;

  ret = 0;
  img->img_ptr = mlx_xpm_file_to_image(all->mlx.mlx_ptr,
				       path,
				       &(img->width),
				       &(img->heigh));
  ret = img->img_ptr == NULL ? 0 : 1;
  if (ret)
    {
      img->data = mlx_get_data_addr(img->img_ptr,
				    &img->bpp,
				    &img->size_line, &img->endian);
    }
  return (ret);
}

void		set_color_texture(t_all *all, t_hit *hit, int *cur_px_calc)
{
  int		x;
  int		y;
  int		z;
  int		pos;
  t_texture	*texture;
  t_obj		*obj;

  obj = hit->obj;
  texture = get_texture(all, obj->path_text);
  if (texture)
    {
      x = cur_px_calc[0] % texture->img.width;
      y = cur_px_calc[1] % texture->img.heigh;
      pos = x * (texture->img.bpp / 8) + (y * texture->img.size_line);
      obj->color.r = (int)texture->img.data[pos];
      obj->color.g = (int)texture->img.data[pos + 1];
      obj->color.b = (int)texture->img.data[pos + 2];
      obj->color.alpha = (int)texture->img.data[pos + 3];
    }
  else
    obj->path_text = NULL;
}

void		add_texture(t_all *all, char *name, char *path)
{
  t_texture	*tmp;
  t_texture	*new_texture;

  new_texture = x_malloc(sizeof(t_texture), "new_texture");
  bzero(new_texture->name, 20);
  my_memcpy(new_texture->name, name, my_strlen(name));
  if (!(load_texture(all, path, &new_texture->img)))
    {
      free (new_texture);
      return ;
    }
  new_texture->next = NULL;
  if (all->ent.texture == NULL)
    all->ent.texture = new_texture;
  else
    {
      tmp = all->ent.texture;
      while (tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = new_texture;
    }
}

void	init_texture(t_all *all)
{
  add_texture(all, "herb", "textures/sol.xpm");
  add_texture(all, "sky", "textures/sky.xpm");
  add_texture(all, "wall", "textures/wall.xpm");
  add_texture(all, "brick", "textures/brick.xpm");
}

t_texture	*get_texture(t_all *all, char *name)
{
  t_texture	*tmp;

  tmp = all->ent.texture;
  while (tmp)
    {
      if (!my_strcmp(tmp->name, name))
	return (tmp);
      tmp = tmp->next;
    }
  return (NULL);
}
