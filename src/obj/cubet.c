/*
** cubet.c for raytracer in /home/camill_n/mul_2013_raytracer
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu Jun  5 21:01:52 2014 camill_n
** Last update Sat Jun  7 17:19:03 2014 Maxime Boguta
*/

#include "struct.h"

void		_ncubet(t_vector *n, double *i, t_obj *obj)
{
  double	tmp[3];
  t_vector	rot;

  tmp[0] = i[0] - obj->pos[0];
  tmp[1] = i[1] - obj->pos[1];
  tmp[2] = i[2] - obj->pos[2];
  rot.x = -obj->rpos[0];
  rot.y = -obj->rpos[1];
  rot.z = -obj->rpos[2];
  _rot_all_inv(NULL, &tmp, &rot);
  n->x = (4. * CUBE(tmp[0])) - (10. * tmp[0]);
  n->y = (4. * CUBE(tmp[1])) - (10. * tmp[1]);
  n->z = (4. * CUBE(tmp[2])) - (10. * tmp[2]);
}

void		init_indice(t_tmp *tmp, t_obj *obj, double *indice)
{
  indice[A] = QUATRO(VX) + QUATRO(VY) + QUATRO(VZ);

  indice[B] = 4 * (CUBE(VX) * XEYE + CUBE(VY) * YEYE + CUBE(VZ) * ZEYE);

  indice[C] = 6 * (CARRE(VX) * CARRE(XEYE) + CARRE(VY) * CARRE(YEYE)
		   + CARRE(VZ) * CARRE(ZEYE));
  indice[C] -= 5. * (CARRE(VX) + CARRE(VY) + CARRE(VZ));

  indice[D] = 4 * (CUBE(XEYE) * VX + CUBE(YEYE) * VY + CUBE(ZEYE) * VZ);
  indice[D] -= 10. * (XEYE * VX + YEYE * VY + (ZEYE * VZ));

  indice[E] = (QUATRO(XEYE) + QUATRO(YEYE) + QUATRO(ZEYE));
  indice[E] -= 5. * (CARRE(XEYE) + CARRE(YEYE) + CARRE(ZEYE));
  indice[E] += tmp->cur->size;

}

void		calc_cubet(t_tmp *tmp, t_obj **obj)
{
  double	k;
  double	indice[5];

  init_indice(tmp, *obj, indice);
  k = equat4_smaller_k(indice);
  if (k < 0)
    return ;
  if ((*obj == NULL) || k < tmp->hit.dist)
    {
      *obj = tmp->cur;
      tmp->hit.dist = k;
    }
}
