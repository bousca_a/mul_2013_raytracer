/*
** shadows.c for rtv1 in /home/boguta_m/rendu/MUL_2013_rtv1
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Fri Feb 28 16:38:00 2014 Maxime Boguta
** Last update Fri Jun  6 18:16:56 2014 Maxime Boguta
*/

#include "struct.h"
#include "colors.h"
#include "flags.h"

void		_move_sha(t_all *main, t_tmp *tmp)
{
  t_vector	rot;

  tmp->ray.start.x = tmp->hit.hit[0] - tmp->cur->pos[0];
  tmp->ray.start.y = tmp->hit.hit[1] - tmp->cur->pos[1];
  tmp->ray.start.z = tmp->hit.hit[2] - tmp->cur->pos[2];
  rot.x = -tmp->cur->rpos[0];
  rot.y = -tmp->cur->rpos[1];
  rot.z = -tmp->cur->rpos[2];
  _rot_all_inv(main, &tmp->ray.start, &rot);
  tmp->ray.dir.x = tmp->lig.x;
  tmp->ray.dir.y = tmp->lig.y;
  tmp->ray.dir.z = tmp->lig.z;
  _rot_all_inv(main, &tmp->ray.dir, &rot);
}

void	_checks_sha(t_all *main, t_tmp *tmp)
{
  tmp->cur = main->ent.obj;
  tmp->hit.dist = 10000;
  while (tmp->cur)
    {
      if (tmp->cur != tmp->hit.obj)
	{
	  _move_sha(main, tmp);
	  _match(main, tmp, &tmp->hit.shadowfr);
	}
      tmp->cur = tmp->cur->next;
    }
}

int	_shadows(t_all *main, t_tmp *tmp, t_light *lig)
{
  tmp->hit.shadowfr = NULL;
  if (main->info.flags & NO_SHADOWS)
    return ;
  _checks_sha(main, tmp);
  if (tmp->hit.dist < 0 || tmp->hit.dist > 1)
    tmp->hit.shadowfr = NULL;
  return (tmp->hit.shadowfr && tmp->hit.dist < 0 || tmp->hit.dist > 1 ?
	  1 : 0);
}
