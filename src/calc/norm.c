/*
** norm.c for rt in /home/boguta_m/rendu/mul_2013_raytracer/src/calc
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Sat Jun  7 17:18:41 2014 Maxime Boguta
** Last update Sun Jun  8 17:44:22 2014 Maxime Boguta
*/

#include "norm.h"
#include "struct.h"

void		set_tab(void (*norm[6])(t_vector *n, double *i, t_obj *obj))
{
  norm[0] = _nsphere;
  norm[1] = _nplane;
  norm[2] = _ncy;
  norm[3] = _ncon;
  norm[4] = _ntore;
  norm[5] = _ncubet;
}

void		get_norm(t_vector *n, double *i, t_obj *obj)
{
  t_vector	rot;
  void		(*norm[6])(t_vector *n, double *i, t_obj *obj);
  int		c;

  c = -1;
  set_tab(norm);
  while (++c < 6 && (c + 1) != obj->type);
  norm[c](n, i, obj);
  if (obj->type != 1 && obj->type != 6)
    {
      rot.x = obj->rpos[0];
      rot.y = obj->rpos[1];
      rot.z = obj->rpos[2];
      _rot_all(NULL, n, &rot);
    }
  _unitize(n);
}
