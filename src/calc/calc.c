/*
** lines.c for rtv1 in /home/boguta_m/rendu/MUL_2013_rtv1
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Tue Feb 25 13:36:01 2014 Maxime Boguta
** Last update Sat Jun  7 22:44:19 2014 Maxime Boguta
*/

#include "flags.h"
#include "struct.h"

void		_trace(t_all *main, t_color *col, t_tmp *tmp)
{
  tmp->cur = NULL;
  tmp->hit.fhit = NULL;
  _init_pov(main, tmp);
  _checks(main, tmp);
  tmp->hit.fhit = tmp->hit.obj;
  if (tmp->hit.obj != NULL)
    _factors(main, &tmp->color, tmp);
  if (tmp->hit.fhit != NULL && !(main->info.flags & NO_EFFECTS)
      && is_reflex(tmp, main))
    _reflex(main, tmp);
}

void		get_pix(t_all *main, int x, int y, t_color *col)
{
  my_memset(col, sizeof(t_color));
  main->info.aa < 1 ? main->info.aa = 1
    : main->info.aa > 32 ? main->info.aa = 32 : 0;
  aa_handling(main, x, y, col);
}
