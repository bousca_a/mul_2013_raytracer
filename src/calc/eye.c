/*
** eye.c for rt in /home/boguta_m/rendu/mul_2013_raytracer
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Sat Jun  7 14:40:06 2014 Maxime Boguta
** Last update Sun Jun  8 14:55:12 2014 Maxime Boguta
*/

#include "struct.h"

void		_eyeprimary(t_tmp *tmp, double x, double y)
{
  tmp->realvec.x = DIMG;
  tmp->realvec.y = ((double)WIDTH / 2) - x;
  tmp->realvec.z = ((double)HEIGHT / 2) - y;
}

void		_eyerotate(t_all *main, t_tmp *tmp)
{
  t_vector	rot;

  rot.x = main->ent.cam.rpos[0];
  rot.y = main->ent.cam.rpos[1];
  rot.z = main->ent.cam.rpos[2];
  _rot_all(main, &tmp->realvec, &rot);
}

void	_eye(t_all *all, double x, double y, t_tmp *tmp)
{
  _eyeprimary(tmp, x, y);
  _eyerotate(all, tmp);
  _unitize(&tmp->realvec);
}
