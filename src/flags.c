/*
** flags.c for rt in /home/boguta_m/rendu/mul_2013_raytracer
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Mon Jun  2 21:19:05 2014 Maxime Boguta
** Last update Sun Jun  8 19:10:38 2014 Maxime Boguta
*/

#include "struct.h"

inline int	_flags(char **av, int j, int i, t_all *all)
{
  my_strcmp(av[j], "-t") == 0 || my_strcmp(av[j], "--threads") == 0
    ? i = is_existant(all, THREADING_OPT, av, j) : 0;
  my_strcmp(av[j], "-AA") == 0 || my_strcmp(av[j], "--threads") == 0
    ? i = is_existant(all, AA_OPT, av, j) : 0;
  my_strcmp(av[j], "-o") == 0 ? i = is_existant(all, GEN_OUTPUT, av, j) : 0;
  my_strcmp(av[j], "-nS") == 0 ?
    i = is_existant(all, NO_SHADOWS, av, j) : 0;
  my_strcmp(av[j], "-nO") == 0 ?
    i = is_existant(all, NEGATIVE_O, av, j) : 0;
  my_strcmp(av[j], "-nE") == 0 ?
    i = is_existant(all, NO_EFFECTS, av, j) : 0;
  my_strcmp(av[j], "-nB") == 0 ? i = is_existant(all, NO_BRIGHT, av, j) : 0;
  my_strcmp(av[j], "-hL") == 0 ?
    i = is_existant(all, SIMPLE_LIGHTS, av, j) : 0;
  my_strcmp(av[j], "-qD") == 0 ? i = is_existant
    (all, NO_SHADOWS | NO_BRIGHT | NO_EFFECTS | SIMPLE_LIGHTS, av, j) : 0;
  my_strcmp(av[j], "-h") == 0 ? i = is_existant(all, HOST, av, j) : 0;
  my_strcmp(av[j], "-c") == 0 ? i = is_existant(all, CLIENT, av, j) : 0;
  return (i);
}

int		check_flag(t_all *all, char **av, int i, int j)
{
  int		sa;

  sa = all->info.flags;
  i = _flags(av, j, i, all);
  if (sa == all->info.flags && i == 0)
    {
      to_out(2, "error : unknown option : '");
      to_out(2, av[j]);
      exit_msg("'.\n", EXIT_FAILURE);
    }
  if (i == 1)
    return (j + 1);
  return (j);
}

void		parse_flags(t_all *all, char **av)
{
  int		i;
  int		j;

  j = 1;
  i = 0;
  all->info.flags = 0;
  all->nb_thread = 8;
  all->info.aa = 1;
  OUTPATH = my_strdup("output");
  while (av[++j])
    j = check_flag(all, av, i, j);
}
