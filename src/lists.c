/*
** lists.c for rtv1 in /home/boguta_m/rendu/mul_2013_raytracer/src
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Thu May 29 14:14:59 2014 Maxime Boguta
** Last update Sun Jun  1 15:41:31 2014 Maxime Boguta
*/

#include "struct.h"

void		add_obj_in_list
(t_all *all, int type, void (*exec)(t_tmp *t, t_obj **obj))
{
  t_all_obj	*tmp;
  t_all_obj	*new_obj;

  new_obj = x_malloc(sizeof(t_all_obj), "new_obj");
  new_obj->type = type;
  new_obj->exec = exec;
  new_obj->next = NULL;
  if (all->all_obj == NULL)
    all->all_obj = new_obj;
  else
    {
      tmp = all->all_obj;
      while (tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = new_obj;
    }
}

void		add_ent_obj(t_all *all, t_obj *obj)
{
  t_obj	*tmp;
  t_obj	*new_obj;

  new_obj = x_malloc(sizeof(t_obj), "new_obj");
  my_memcpy(new_obj, obj, sizeof(t_obj));
  new_obj->next = NULL;
  if (all->ent.obj == NULL)
    all->ent.obj = new_obj;
  else
    {
      tmp = all->ent.obj;
      while (tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = new_obj;
    }
}

void		add_ent_lig(t_all *all, t_light *light)
{
  t_light	*tmp;
  t_light	*new_light;

  new_light = x_malloc(sizeof(t_light), "new_light");
  my_memcpy(new_light, light, sizeof(t_light));
  new_light->next = NULL;
  if (all->ent.light == NULL)
    all->ent.light = new_light;
  else
    {
      tmp = all->ent.light;
      while (tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = new_light;
    }
}
