/*
** send.c for rt in /home/camill_n/mul_2013_raytracer
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Fri Jun  6 20:02:06 2014 camill_n
** Last update Sun Jun  8 01:46:09 2014 camill_n
*/

#include "struct.h"

void	send_my_xml(char *src, t_host *host)
{
  int	ret;

  ret = 0;
  while (ret < PAQUET_SIZE)
    ret += write(host->sock, src + ret, PAQUET_SIZE - ret);
}

void	get_my_xml(t_all *all, char *dst)
{
  int	ret;

  ret = 0;
  while (ret < PAQUET_SIZE)
    ret += read(all->cluster.sock_client.sock, dst + ret, PAQUET_SIZE - ret);
}

void	send_xml(t_all *all, t_host *host, int nb_c)
{
  int	fd;
  int	ret;
  char	buff[PAQUET_SIZE];
  int	val[2];
  int	ret2;

  val[0] = all->cluster.nb_h;
  val[1] = nb_c;
  send(host->sock, val, 2 * sizeof(int), 0);
  fd = open(all->info.file, O_RDONLY);
  if (fd > 0)
    {
      ret = read(fd, buff, PAQUET_SIZE);
      buff[ret] = 0;
      buff[ret + 1] = '\004';
      send_my_xml(buff, host);
    }
  else
    {
      my_printf("File doesn't exist..\n");
      exit(EXIT_FAILURE);
    }
}
