/*
** listen.c for rt in /home/camill_n/mul_2013_raytracer
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Fri Jun  6 16:13:16 2014 camill_n
** Last update Sun Jun  8 23:03:10 2014 Maxime Boguta
*/

#include "struct.h"

void		wait_for_xml(t_all *all)
{
  int		info[2];
  t_sock	*sock;
  char		*xml;
  int		ret;

  sock = &all->cluster.sock_client;
  xml = x_malloc(sizeof(char) * PAQUET_SIZE, xml);
  ret = 0;
  my_memset(xml, PAQUET_SIZE * sizeof(char));
  if ((ret = recv(sock->sock, info, sizeof(int) * 2, 0)) > 0)
    {
      all->cluster.nb_h = info[0];
      all->cluster.nb_c = info[1];
    }
  get_my_xml(all, xml);
  all->cluster.content = xml;
}

void		add_client(t_all *all, fd_set *rdfs)
{
  t_sock	*sock;
  int		size;

  my_printf("Détection d'un nouveau client\n");
  sock = &all->cluster.sock_client;
  bzero(&sock->info, sizeof(sock->info));
  size = 0;
  sock->sock = accept(all->cluster.sock.sock,
		      (struct sockaddr *)&sock->info,
		      (socklen_t *)&size);
  if (sock->sock == -1)
    exit_msg("socket error.", EXIT_FAILURE);
  ++all->cluster.client_connect;
}

void		wait_for_client(t_all *all)
{
  t_sock	*sock;
  fd_set	rdfs;
  int		highest_sock;

  sock = &all->cluster.sock;
  all->cluster.client_connect = 0;
  my_printf("Waiting for client...\n");
  highest_sock = sock->sock;
  while (!all->cluster.client_connect)
    {
      FD_ZERO(&rdfs);
      FD_SET(all->cluster.sock.sock, &rdfs);
      if (select(highest_sock + 1, &rdfs, NULL, NULL, NULL) < 0)
	exit_msg("socket error", EXIT_FAILURE);
      if (FD_ISSET(all->cluster.sock.sock, &rdfs))
	add_client(all, &rdfs);
      usleep(100);
    }
  my_printf("A client is now connected\n");
}
