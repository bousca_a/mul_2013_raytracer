/*
** util.c for rt in /home/camill_n/mul_2013_raytracer
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Fri Jun  6 15:02:30 2014 camill_n
** Last update Sun Jun  8 23:05:34 2014 Maxime Boguta
*/

#include "struct.h"

int		connect_server(t_all *all, t_host *host, char *srv)
{
  bzero(&host->info, sizeof(host->info));
  host->addr_host = inet_addr(srv);
  if ((long)host->addr_host != (long)-1)
    bcopy(&host->addr_host, &host->info.sin_addr, sizeof(host->addr_host));
  else
    {
      host->host = gethostbyname(srv);
      if (host->host == NULL)
	{
	  my_printf("Server is not reachable: %s\n", srv);
	  return (0);
	}
      bcopy(host->host->h_addr, &host->info.sin_addr, host->host->h_length);
    }
  host->info.sin_port = htons(PORT);
  host->info.sin_family = AF_INET;
  if ((host->sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
      my_printf("Fail during the creation of hoste: %s\n", srv);
      return (0);
    }
  if (connect(host->sock, (struct sockaddr *)&host->info,
	      sizeof(host->info)) < 0)
    my_printf("Connexion refused by %s\n", srv);
  return (1);
}

t_host		*add_host
(t_all *all, int start_interval, int size_interval, char *srv)
{
    t_host	*tmp;
    t_host	*new_host;
    int		ret;

    new_host = x_malloc(sizeof(t_host), "host");
    new_host->start_interval = start_interval;
    new_host->size_interval = size_interval;
    new_host->next = NULL;
    ret = connect_server(all, new_host, srv);
    if (!ret)
      {
	free(new_host);
	return (NULL);
      }
    if (all->cluster.host == NULL)
      all->cluster.host = new_host;
    else
      {
        tmp = all->cluster.host;
        while (tmp->next != NULL)
	  tmp = tmp->next;
        tmp->next = new_host;
      }
    ++all->cluster.nb_host;
    return (new_host);
}

void		del_host(t_all *all, t_host *host)
{
  t_host	*tmp;

  if (!all->cluster.nb_host)
    return ;
  if (all->cluster.host != NULL && all->cluster.host->next == NULL)
    all->cluster.host = NULL;
  else if (all->cluster.host == host)
    all->cluster.host = host->next;
  else
    {
      tmp = all->cluster.host;
      while (tmp->next != host)
	tmp = tmp->next;
      if (host->next != NULL)
	tmp->next = host->next;
      else
	tmp->next = NULL;
    }
    --(all->cluster.nb_host);
    close(host->sock);
    free(host);
}

void		init_srv_socket(t_all *all)
{
  int		opt;
  t_sock	*sock;

  sock = &all->cluster.sock;
  opt = 1;
  bzero(&sock->info, sizeof(sock->info));
  if ((sock->sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    exit_msg("socket error", EXIT_FAILURE);
  sock->info.sin_family = AF_INET;
  sock->info.sin_port = htons(PORT);
  sock->info.sin_addr.s_addr = INADDR_ANY;
  setsockopt(sock->sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
  if (bind(sock->sock, (struct sockaddr *)&sock->info,
  	   sizeof(sock->info)) == -1)
    exit_msg("socket error", EXIT_FAILURE);
  if (listen(sock->sock, 1) < 0)
    exit_msg("socket error", EXIT_FAILURE);
  my_printf("\033[32mDone.\033[0m\n");
}
