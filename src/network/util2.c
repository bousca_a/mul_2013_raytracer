/*
** util2.c for rt in /home/camill_n/mul_2013_raytracer
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Fri Jun  6 19:39:52 2014 camill_n
** Last update Sun Jun  8 23:03:52 2014 Maxime Boguta
*/

#include "struct.h"

void		send_result_to_client(t_all *all, int size_paq, int size_tot)
{
  int		i;
  t_img		*img;
  t_cluster	*clu;
  int		size;
  int		start;
  unsigned char	buff[size_paq];

  clu = &all->cluster;
  img = &all->img;
  size = size_tot / size_paq;
  start = size_tot * clu->nb_c;
  while (i < size)
    {
      memcpy(buff, &all->img.data[start + (i * size_paq)], size_paq);
      send(clu->sock_client.sock, buff, size_paq, MSG_CONFIRM);
      ++i;
    }
}

void		get_result_from_host(t_all *all, int size_paq, int size_tot)
{
  int		i;
  t_img		*img;
  int		size;
  int		start;
  unsigned char	buff[size_paq];
  int		j;
  t_host	*host;

  host = all->cluster.host;
  img = &all->img;
  i = 1;
  size = size_tot / size_paq;
  while (host)
    {
      start = size_tot * i;
      j = 0;
      while (j < size)
	{
	  recv(host->sock, buff, size_paq, MSG_WAITALL);
	  memcpy(&all->img.data[start + (j * size_paq)], buff, size_paq);
	  ++j;
	}
      ++i;
      host = host->next;
    }
}

void	init_cluster(t_cluster *cluster)
{
  cluster->content = NULL;
  cluster->nb_host = 0;
  cluster->mode_host = 0;
  cluster->mode_client = 0;
  cluster->client_connect = 0;
  cluster->host = NULL;
  cluster->nb_h = 0;
}

void	active_server(t_all *all)
{
  my_printf("Server online\n");
  all->cluster.mode_host = 1;
  init_srv_socket(all);
  wait_for_client(all);
  wait_for_xml(all);
  my_printf("%s\n", all->cluster.content);
  close(all->cluster.sock.sock);
}

void		active_client(t_all *all)
{
  t_host	*host;

  all->cluster.nb_h = 2;
  all->cluster.nb_c = 0;
  my_printf("Client active\n");
  all->cluster.mode_client = 1;
  host = add_host(all, 0, 500, "127.0.0.1");
  send_xml(all, host, 1);
  close(all->cluster.sock.sock);
}
