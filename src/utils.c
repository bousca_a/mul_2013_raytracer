/*
** utils.c for rt in /home/camill_n/mul_2013_raytracer
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat Jun  7 05:09:16 2014 camill_n
** Last update Sun Jun  8 22:58:46 2014 Maxime Boguta
*/

#include "struct.h"

int		amount_done(int	*cpt, int nb_thread, int size_tot)
{
  int		r;
  int		v;
  int		i;

  i = 0;
  r = 0;
  v = 0;
  while (--nb_thread > -1)
    {
      r += cpt[nb_thread];
      ++i;
    }
  return (((float)r / size_tot) * 100);
}

void		amount_disp(t_all *all, int nb_thread)
{
  int		tmp;
  int		i;
  int		tot;

  tmp = 0;
  i = 0;
  tot = WIDTH * HEIGHT;
  if (all->cluster.nb_h > 0)
    tot /= all->cluster.nb_h;
  if (all->cluster.nb_h > 0)
    tot /= all->cluster.nb_h;
  while ((i = amount_done(all->info.cpt, nb_thread, tot)) < 99)
    {
      if (tmp != i)
	{
	  tmp = i;
	  my_putchar('\r');
	  my_printf("Computing scene... (\033[32m%d\033[0m %%)", i);
	}
    }
}

void		init_calc(t_all *all, int *size_tot, int *i, int nb_thread)
{
  *size_tot = WIDTH * HEIGHT;
  if (all->cluster.mode_host || all->cluster.mode_client)
    *size_tot /= all->cluster.nb_h;
  *i = 0;
  all->thread_s = -1;
  all->info.cpt = x_malloc(sizeof(int) * nb_thread, "nbt");
  my_memset(all->info.cpt, sizeof(int) * nb_thread);
  all->thread_interval = *size_tot / nb_thread;
  my_printf("Rendering (%d threads)\n", nb_thread);
  all->nb_thread = nb_thread;
}

void		sub_calc(t_all *all, int nb_thread)
{
  pthread_t	thread[nb_thread];
  int		i;
  int		check;
  int		size_tot;

  init_calc(all, &size_tot, &i, nb_thread);
  while (i < nb_thread)
    {
      if (all->thread_s == -1)
	{
	  all->thread_free = i + 1;
	  if (all->cluster.mode_host || all->cluster.mode_client)
	    all->thread_s = (all->cluster.nb_c * size_tot) + i;
	  else
	    all->thread_s = i;
	  check = pthread_create(&(thread[i]), NULL, calc_scene, all);
	  check == 0 ? ++i : 0;
	}
      usleep(1);
    }
  amount_disp(all, nb_thread);
  i = 0;
  while (i < nb_thread)
    pthread_join(thread[i++], 0);
}
