/*
** hook.c for raytracer in /home/bousca_a/rendu/MUL_2013_raytracer
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Wed May 28 18:01:05 2014 Antonin Bouscarel
** Last update Wed May 28 18:15:42 2014 Antonin Bouscarel
*/

#include "struct.h"

int	gere_key(int keycode, t_all *all)
{
  if (keycode == 65307)
    {
      mlx_destroy_window(MLX.mlx_ptr, MLX.win_ptr);
      exit_msg("\nProgram received exit signal, exiting...\n");
    }
  return (0);
}

int	expose_hook(t_all *all)
{
  mlx_put_image_to_window(MLX.mlx_ptr, MLX.win_ptr, IMG.img_ptr, 0, 0);
  return (0);
}

void	hook_event(t_all *all)
{
  mlx_key_hook(MLX.win_ptr, gere_key, all);
  mlx_expose_hook(MLX.win_ptr, expose_hook, all);
}
