/*
** parse_entity.c for rtv in /home/bousca_a/rendu/parser_rtv
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Wed May 21 17:44:32 2014 Antonin Bouscarel
** Last update Sun Jun  8 23:08:11 2014 Maxime Boguta
*/

#include "struct.h"
#include "global.h"

void	add_type_entity(t_obj *obj, char *str)
{
  char	*tofree;

  my_strcmp(tofree = find_content(str, "<type>", "</type>"),
	    "sphere") == 0 ? obj->type = SPHERE : 0;
  free(tofree);
  my_strcmp(tofree = find_content(str, "<type>", "</type>"),
	    "plan") == 0 ? obj->type = PLAN : 0;
  free(tofree);
  my_strcmp(tofree = find_content(str, "<type>", "</type>"),
	    "cylindre") == 0 ? obj->type = CYLINDRE : 0;
  free(tofree);
  my_strcmp(tofree = find_content(str, "<type>", "</type>"),
	    "cone") == 0 ? obj->type = CONE : 0;
  free(tofree);
  my_strcmp(tofree = find_content(str, "<type>", "</type>"),
	    "tore") == 0 ? obj->type = TORE : 0;
  free(tofree);
  my_strcmp(tofree = find_content(str, "<type>", "</type>"),
	    "cube_t") == 0 ? obj->type = CUBE_T : 0;
  free(tofree);
}

void	add_pos(t_obj *obj, char *str)
{
  char	**pos;
  char	**rpos;
  char	*tofree;

  pos = my_wordtab(tofree = find_content(str, "<pos>", "</pos>"), ':');
  free(tofree);
  rpos = my_wordtab(tofree = find_content(str, "<rvec>", "</rvec>"), ':');
  free(tofree);
  if ((pos == NULL || rpos == NULL)
      || get_sizetab(pos) < 3 || get_sizetab(rpos) < 3)
    return ;
  bzero(obj->pos, sizeof(obj->pos));
  bzero(obj->rpos, sizeof(obj->rpos));
  obj->pos[0] = my_getnbrf(pos[0]);
  obj->pos[1] = my_getnbrf(pos[1]);
  obj->pos[2] = my_getnbrf(pos[2]);
  obj->rpos[0] = my_getnbrf(rpos[0]);
  obj->rpos[1] = my_getnbrf(rpos[1]);
  obj->rpos[2] = my_getnbrf(rpos[2]);
  my_free_wordtab(rpos);
  my_free_wordtab(pos);
}

void	add_color(t_obj *obj, char *color, t_all *all)
{
  char	**rgb;

  rgb = my_wordtab(color, ':');
  if (rgb == NULL || get_sizetab(rgb) < 4)
    return ;
  obj->color.r = my_getnbr(rgb[0]);
  obj->color.g = my_getnbr(rgb[1]);
  obj->color.b = my_getnbr(rgb[2]);
  obj->color.alpha = my_getnbr(rgb[3]);
  if (all->info.flags & NEGATIVE_O)
    {
      obj->color.r = 255 - obj->color.r;
      obj->color.g = 255 - obj->color.g;
      obj->color.b = 255 - obj->color.b;
    }
  my_free_wordtab(rgb);
}

void	add_entity(t_all *all, char *str)
{
  t_obj	obj;
  char	*tofree;

  init_obj(&obj);
  add_type_entity(&obj, str);
  add_pos(&obj, str);
  add_color(&obj, tofree = find_content(str, "<color>", "</color>"), all);
  free(tofree);
  obj.size = my_getnbrf(tofree = find_content(str, "<size>", "</size>"));
  free(tofree);
  obj.path_text = (find_content(str, "<text>", "</text>"));
  obj.reflex = my_getnbr(tofree = find_content(str, "<reflex>", "</reflex>"));
  free(tofree);
  obj.shine = my_getnbr(tofree = find_content(str, "<shine>", "</shine>"));
  free(tofree);
  obj.size2 = my_getnbrf(tofree = find_content(str, "<size2>", "</size2>"));
  free(tofree);
  obj.cons = my_getnbrf(tofree = find_content(str, "<const>", "</const>"));
  free(tofree);
  obj.specular =
    my_getnbrf(tofree = find_content(str, "<specular>", "</specular>"));
  free(tofree);
  add_ent_obj(all, &obj);
}

void	add_obj(t_all *all, char *str)
{
  char	*tmp;

  while (*str)
    {
      tmp = get_content(str, "<obj>", "</obj>");
      if (tmp)
	add_entity(all, tmp);
      free(tmp);
      ++str;
    }
}
