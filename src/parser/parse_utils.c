/*
** parse_utils.c for rtv in /home/bousca_a/rendu/parser_rtv
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Wed May 21 17:42:45 2014 Antonin Bouscarel
** Last update Sat Jun  7 17:14:02 2014 Maxime Boguta
*/

#include <string.h>
#include "struct.h"

int	is_to_epur(char *str)
{
  while (*str && (*str == ' ' || *str == '\n' || *str == '\t'))
    ++str;
  if (*str)
    return (0);
  return (1);
}

char	*epur_str(char *str)
{
  int	i;

  i = 0;
  while (str[0] && (str[0] == ' ' || str[0] == '\n' || str[0] == '\t'))
    my_strcpy(str, str + 1);
  while (str[i] && !is_to_epur(str + i))
    ++i;
  str[i] = '\0';
  return (str);
}

char	*get_content(char *rss, char *start, char *end)
{
  char	*start_ptr;
  char	*content;

  start_ptr = NULL;
  if (my_strncmpp(rss, start, my_strlen(start)))
    return (NULL);
  rss += my_strlen(start);
  start_ptr = rss;
  while (*rss && my_strncmpp(rss, end, my_strlen(end)))
    ++rss;
  if (!*rss)
    return (NULL);
  *rss = '\0';
  if (!(content = my_strdup(start_ptr)))
    return (NULL);
  *rss = '<';
  return (epur_str(content));
}

char	*find_content(char *str, char *start, char *end)
{
  char	*tmp;

  tmp = NULL;
  while (*str)
    {
      tmp = get_content(str, start, end);
      if (tmp)
	return (tmp);
      free(tmp);
      ++str;
    }
  return (NULL);
}
