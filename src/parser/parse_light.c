/*
** parse_light.c for rtv in /home/bousca_a/rendu/parser_rtv
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Wed May 21 17:45:22 2014 Antonin Bouscarel
** Last update Sun Jun  8 20:09:52 2014 Maxime Boguta
*/

#include "global.h"
#include "struct.h"

void	add_pos_light(t_light *lig, char *data)
{
  char	**pos;
  char	*tofree;

  pos = my_wordtab(data, ':');
  if (pos == NULL || get_sizetab(pos) < 3)
    return ;
  lig->pos[0] = my_getnbrf(pos[0]);
  lig->pos[1] = my_getnbrf(pos[1]);
  lig->pos[2] = my_getnbrf(pos[2]);
  my_free_wordtab(pos);
}

void	add_color_light(t_light *lig, char *data)
{
  char	**color;

  if (((color = my_wordtab(data, ':')) == NULL) || get_sizetab(color) < 3)
    return ;
  lig->color.r = my_getnbr(color[0]);
  lig->color.g = my_getnbr(color[1]);
  lig->color.b = my_getnbr(color[2]);
  my_free_wordtab(color);
}

void		add_new_light(t_all *all, char *str)
{
  t_light	lig;
  char		*tofree;

  init_lig(&lig);
  my_strcmp(tofree = find_content(str, "<shape>", "</shape>"),
	    "box") == 0 ? lig.shape = 1 : 0;
  my_strcmp(tofree = find_content(str, "<type>", "</type>"),
	    "specular") == 0 ? lig.type = SPHERE : 0;
  free(tofree);
  add_pos_light(&lig, tofree = find_content(str, "<pos>", "</pos>"));
  free(tofree);
  lig.size = my_getnbrf(tofree = find_content(str, "<size>", "</size>"));
  free(tofree);
  add_color_light(&lig, tofree = find_content(str, "<color>", "</color>"));
  free(tofree);
  lig.strengh = my_getnbrf
    (tofree = find_content(str, "<strengh>", "</strengh>"));
  free(tofree);
  lig.soft = my_getnbrf
    (tofree = find_content(str, "<soft>", "</soft>"));
  free(tofree);
  add_ent_lig(all, &lig);
}

void	add_light(t_all *all, char *str)
{
  char	*tmp;

  while (*str)
    {
      tmp = get_content(str, "<light>", "</light>");
      if (tmp)
	add_new_light(all, tmp);
      free(tmp);
      ++str;
    }
}
