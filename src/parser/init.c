/*
** init.c for rt in /home/boguta_m/rendu/mul_2013_raytracer
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Thu Jun  5 15:38:22 2014 Maxime Boguta
** Last update Sun Jun  8 20:14:31 2014 Maxime Boguta
*/

#include "struct.h"

void	init_obj(t_obj *obj)
{
  my_memset(obj, sizeof(t_obj));
  obj->color.r = 255;
  obj->color.g = 255;
  obj->color.b = 255;
  obj->color.alpha = 0;
  obj->size = 5;
  obj->size2 = 1;
  obj->shine = 0;
  obj->special = 0;
  obj->reflex = 0;
}

void	init_lig(t_light *lig)
{
  lig->soft = 1;
  lig->type = 0;
  lig->shape = 0;
  lig->size = 1;
  lig->pos[0] = 0;
  lig->pos[1] = 0;
  lig->pos[2] = 0;
  lig->color.r = 255;
  lig->color.g = 255;
  lig->color.b = 255;
  lig->color.alpha = 0;
  lig->strengh = 100;
}
