/*
** main.c for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Mon Feb 10 20:50:26 2014 camill_n
** Last update Sun Jun  8 22:41:46 2014 Maxime Boguta
*/

#include <pthread.h>
#include <signal.h>
#include "global.h"
#include "struct.h"
#include "flags.h"

void	init_network(t_all *all)
{
  if (all->info.flags & CLIENT)
    active_client(all);
}

void	case_online(t_all *all, char **av)
{
  if (av[1] && av[2] && !my_strcmp("-c", av[2]))
    get_result_from_host
      (all, 102400, (all->img.size_line * HEIGHT) / all->cluster.nb_h);
  else if (av[1] && !my_strcmp("-h", av[1]))
    send_result_to_client
      (all, 102400, (all->img.size_line * HEIGHT) / all->cluster.nb_h);
  if (!all->cluster.mode_host)
    {
      save_as_bmp(all);
      init_win(all);
      hook_event(all);
      mlx_loop(all->mlx.mlx_ptr);
    }
}

int	main(int ac, char **av)
{
  t_all	all;

  if (ac >= 2)
    all.info.file = av[1];
  else
    usage();
  init_cluster(&all.cluster);
  all.ent.obj = NULL;
  all.ent.light = NULL;
  srand48(time(NULL));
  parse_flags(&all, av);
  if (!my_strcmp(av[1], "-h"))
    active_server(&all);
  if (av[1] && !my_strcmp(av[2], "-c"))
    active_client(&all);
  parse_file(&all);
  init_all(&all);
  sub_calc(&all, all.nb_thread);
  case_online(&all, av);
  return (0);
}
