rt - A simple XML-like file to a raytraced image

USAGE :

      rt <scene file> [-t [THREAD_AMT]] [-o [OUTPUT FILE]] [-qD -nS -nB -nE]

INPUT FILE :

      Rt's input file is an XML-like file, describing the scene with it's objects, lights and camera position.
Three main classes are described in, starting with "<class>" and ending with it's closing tag "</class>" for each class/subclass,
each containaing subclasses describing the main class atributes.

CLASSES :

     <obj> : Main class describing an object in the scene.

     <cam> : Kind of mandatory, describing the scene's POV.

     <light> : One is necessery, describing the lights, how they should light the scene up.

     SUBCLASSES :
			OBJECTS - LIGHTS  <type>  : Describing object's type, as it's shape. "box" for an finish-shaped light.
				     	  	    Ex : "plane" for a plane.

			LIGHTS		  <soft>  : If type = "box", define how many rays will be casted to the light sources, to soft lightning and
					  	    shadowing intent. Note : This value is really performance harming.

			OBJECTS, LIGHTS   <color> : Four composants separated by a semicolon ':',
				 	  	    to the following syntax Red:Green:Blue:Alpha.
						    Each color composant can be up to 255. 255 to R, G and B stands as white color.
						    Ex : 250:100:60:0.

			OBJECTS		  <const> : Static constant for some objects, such as cones.

			ALL 	 	  <pos>   : X, Y, and Z position in space for the related object, separated by a semicolon ':'.
			    		  	    Ex : 10:0:5

			OBJECTS, CAMERA	  <rvec>  : Rotational composants in X, Y and Z for the object, separated by a semicolon ':'.
			    		  	    Ex : 15:0:77

			OBJECTS, LIGHT	  <size>  : Static constant for most objects, describing it's size, one numeric value expected.
				     	  	    Ex : 10.

			OBJECTS		  <size2> : Second static constant for most objects, describing it's size, one numeric value expected.
				     	  	    Ex : 10.


OPTIONS :

     -t (--threads) [OUTPUT FILE] : Takes THREAD_AMT as parameter, threads created to render the scene (default and best is 8 threads on a 8 core processor's computer).
     -o [OUTPUT FILE] : uses OUTPUT FILE as a .bmp output of the rendered scene, default set to "output.bmp".

     RENDER OPTIONS :

     	   -AA : Anti-Aliasing : set anti-aliasing amount.
	   -qD : Quick Draw : disable all shadows, brightness and effects for performance intents.
	   -nS : No Shadows : disable all shadows for performance intents.
	   -hL : Hard Lightning : kill all special light specs for performance intents.
	   -nB : No Brightness : disable all brightness effects for performance intents.
	   -nE : No Effects : disable all post-brightness render for performance intents.

EPITECH - 2014 - rt - Maxime Boguta, Antonin Bouscarel, Geoffrey Rivori, Nicolas Camilli, Tugdual Fleury, Loïc Froment
