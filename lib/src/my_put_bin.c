/*
** my_put_bin.c for printf in /home/camill_n/rendu/PSU_2018_my_printf
** 
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
** 
** Started on  Thu Nov 14 20:06:36 2013 Nicolas Camilli
** Last update Sun Nov 17 10:23:26 2013 Nicolas Camilli
*/

#include "../includes/func.h"

void		manage_put_bin(va_list ap)
{
  unsigned int	tmp;

  tmp = va_arg(ap, unsigned int);
  if (tmp == 0)
    my_putchar('0');
  else
    my_put_bin(tmp);
}

void	manage_put_nonimp(va_list ap)
{
  my_put_nonimp(va_arg(ap, char *));
}

void	my_put_bin(unsigned int nb)
{
  int	tmp;

  if (nb > 0)
    {
      tmp = nb % 2;
      nb = nb / 2;
      my_put_bin(nb);
      if (tmp < 10)
	my_putchar(tmp + 48);
    }
}

void	my_put_nonimp(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] < 32 || str[i] >= 127)
	{
	  if (str[i] > 128)
	    my_putchar('\n');
	  my_putstr("\\");
	  if (str[i] < 10)
	    my_putstr("00");
	  else if (str[i] < 32)
	    my_putstr("0");
	  my_put_octal((unsigned int)(str[i]));
	}
      else
	my_putchar(str[i]);
      i++;
    }
}
