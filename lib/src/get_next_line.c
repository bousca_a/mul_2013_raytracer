/*
** get_next_line.c for get_next_ in /home/camill_n/rendu/PSU_2013_minishell1
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu Jan 23 13:28:26 2014 camill_n
** Last update Wed Feb 19 13:15:53 2014 bousca_a
*/

#include "global.h"

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    ++i;
  return (i);
}

char	*my_strncat(char *src, int i, int j)
{
  int	k;
  char	*new_str;

  k = 0;
  new_str = x_malloc((my_strlen(src) + 1) * sizeof(char), "gnl");
  while (i < j)
    new_str[k++] = src[i++];
  new_str[k] = '\0';
  return (new_str);
}

char	*check_save(char **buffer_save, char *buffer_s_copy)
{
  int	i;
  char	*tmp;

  i = 0;
  while (buffer_s_copy[i] != '\0')
    {
      if (buffer_s_copy[i] == '\n' || buffer_s_copy[i + 1] == '\0')
	{
	  tmp = my_strncat(buffer_s_copy, 0, i);
	  *buffer_save += (i + 1);
	  return (tmp);
	}
      i = i + 1;
    }
  return (NULL);
}

char		*get_next_line(const int fd)
{
  static char	*buffer_save = "";
  char		current_buff[SIZE_READ];
  int		ret;
  char		*tmp;
  int		end;

  end = 0;
  if ((tmp = check_save(&buffer_save, buffer_save)) != NULL)
    return (tmp);
  while ((ret = read(fd, current_buff, SIZE_READ)) > 0)
    {
      current_buff[ret] = '\0';
      buffer_save = my_strcat(buffer_save, current_buff);
      if ((tmp = check_save(&buffer_save, buffer_save)) != NULL)
	return (tmp);
    }
  if (my_strlen(buffer_save) > 0 && end++ == 0)
    return (my_strcat(buffer_save, ""));
  return (NULL);
}
