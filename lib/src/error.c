/*
** error.c for error in /home/boguta_m/rendu/mul_2013_raytracer
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Mon Jun  2 20:32:03 2014 Maxime Boguta
** Last update Mon Jun  2 20:33:08 2014 Maxime Boguta
*/

#include "stdlib.h"

void		exit_msg(char *m, int e)
{
  if (e != 0)
    {
      to_out(2, m);
      to_out(2, "\n");
    }
  exit(e);
}
