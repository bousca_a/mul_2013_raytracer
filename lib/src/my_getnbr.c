/*
** my_getnbr.c for my_getnbr.c in /home/camill_n/rendu/Piscine-C-lib/my
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Tue Oct  8 11:40:54 2013 Nicolas Camilli
** Last update Mon Jun  2 23:49:40 2014 Maxime Boguta
*/

#include "func.h"

int             is_neg(char *str)
{
  int           i;
  int           counter;

  i = 0;
  counter = 0;
  while (str[i] > '9' || str[i] < '0')
    {
      if (str[i] == '-')
	counter++;
      i++;
    }
  if (counter % 2 != 0)
    return (1);
  else    return (0);
}

int             is_num(char l)
{
  if (l >= '0' && l <= '9')
    return (1);
  else
    return (0);
}

int             my_getnbr(char *str)
{
  int           i;
  int           nbr;

  nbr = 0;
  i = 0;
  if (str == 0)
    return (0);
  while (is_num(str[i]) == 0)
    i++;
  while (is_num(str[i]) == 1)
    {
      nbr = ((nbr * 10) + (str[i] - 48));
      i++;
    }
  if (is_neg(str) == 1)
    nbr = -nbr;
  return (nbr);
}
