/*
** my_put_hexa.c for my_put_hexa in /home/camill_n/rendu/PSU_2018_my_printf
** 
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
** 
** Started on  Thu Nov 14 18:55:54 2013 Nicolas Camilli
** Last update Sun Nov 17 10:26:32 2013 Nicolas Camilli
*/

#include "../includes/func.h"

void		manage_put_hexa(va_list ap)
{
  unsigned int	tmp;

  tmp = va_arg(ap, unsigned int);
  if (tmp == 0)
    my_putchar('0');
  else
    my_put_hexa(tmp, 0);
}

void	manage_put_hexA(va_list ap)
{
  unsigned int	tmp;

  tmp = va_arg(ap, unsigned int);
  if (tmp == 0)
    my_putchar('0');
  else
    my_put_hexa(tmp, 1);
}

void	manage_put_addrptr(va_list ap)
{
  my_putstr("0x7fff");
  my_put_hexa(va_arg(ap, unsigned int), 0);
}

void	my_put_hexa(unsigned int nb, int mode)
{
  int	tmp;

  if (nb > 0)
    {
      tmp = nb % 16;
      nb = nb / 16;
      my_put_hexa(nb, mode);
      if (tmp < 10)
	my_putchar(tmp + 48);
      else if (mode == 0)
	my_putchar(97 + tmp - 10);
      else
	my_putchar(65 + tmp - 10);
    }
}
