/*
** my_putstr.c for my_putstr in /home/camill_n/rendu/Piscine-C-lib/my
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Mon Oct 21 07:45:59 2013 Nicolas Camilli
** Last update Fri Nov 15 14:26:40 2013 Nicolas Camilli
*/

#include "../includes/func.h"

void	manage_putstr(va_list ap)
{
  my_putstr(va_arg(ap, char *));
}

void	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      my_putchar(str[i]);
      i++;
    }
}
