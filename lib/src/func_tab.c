/*
** func_tab.c for my_printf in /home/camill_n/rendu/PSU_2018_my_printf
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Thu Nov 14 15:11:15 2013 Nicolas Camilli
** Last update Sat Jun  7 02:09:51 2014 Maxime Boguta
*/

#include "../includes/func.h"

int	get_id_func(const char *str, int *i, t_func **tab)
{
  int	j;
  char	c;

  j = 0;
  c = str[*i + 1];
  if (c == 'l' && str[*i + 2] != '\0' && str[*i + 2] == 'd')
    c = 'z';
  if (c == '#')
    {
      c = str[*i + 2];
      if (str[*i + 2] == 'x' || str[*i + 2] == 'X')
	{
	  my_putchar('0');
	  my_putchar(str[*i + 2]);
	}
      *i += 1;
    }
  while (j < 12)
    {
      if ((*tab)[j].flag == c)
	return (j);
      j++;
    }
  return (-1);
}

void	set_flag(int i, char c, t_func **tab, void (*func_tab)(va_list ap))
{
  (*tab)[i].flag = c;
  (*tab)[i].func_tab = func_tab;
}

void	set_func(t_func **tab)
{
  *tab = malloc(12 * sizeof(t_func));
  if (tab != NULL)
    {
      set_flag(0, 'c', tab, &manage_putchar);
      set_flag(1, 'd', tab, &manage_put_nbr);
      set_flag(2, 's', tab, &manage_putstr);
      set_flag(3, 'o', tab, &manage_put_octal);
      set_flag(4, 'u', tab, &manage_put_unsigned);
      set_flag(5, 'x', tab, &manage_put_hexa);
      set_flag(6, 'X', tab, &manage_put_hexA);
      set_flag(7, 'b', tab, &manage_put_bin);
      set_flag(8, 'S', tab, &manage_put_nonimp);
      set_flag(9, 'p', tab, &manage_put_addrptr);
      set_flag(10, 'i', tab, &manage_put_nbr);
      set_flag(11, 'z', tab, &manage_put_long);
    }
}
