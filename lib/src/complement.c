/*
** complement.c for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Mon Feb 10 20:51:11 2014 camill_n
** Last update Sat Jun  7 17:12:49 2014 Maxime Boguta
*/

#include "global.h"

void	my_strcatt(char *s1, char *s2)
{
  int	i;
  int	j;

  i = 0;
  j = -1;
  while (s1[i])
    i++;
  while (s2[++j])
    s1[i++] = s2[j];
  s1[i] = 0;
}

char	*my_strcat(char *s1, char *s2)
{
  char	*new;
  int	i;
  int	j;

  i = 0;
  j = 0;
  new = x_malloc(my_strlen(s1) + my_strlen(s2) + 1, "new");
  while (s1[i] != '\0')
    new[i++] = s1[j++];
  j = 0;
  while (s2[j] != '\0')
    new[i++] = s2[j++];
  new[i] = '\0';
  return (new);
}

int	is_alnum(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] < 48 && str[i] != ' ' && str[i] != '#' && str[i] != '/' &&
	  str[i] != '.')
	return (0);
      if (str[i] > 57 && str[i] < 65)
	return (0);
      if (str[i] > 90 && str[i] < 97)
	return (0);
      if (str[i] > 127)
	return (0);
      ++i;
    }
  return (1);
}

int	my_strncmp(char *s1, char *s2, int i, int j)
{
  if (!s1 || !s2)
    return (-4);
  if (i - j == 0)
    return (1);
  while (i < j && s1[i] != '\0' && s2[i] != '\0')
    {
      if (s1[i] != s2[i])
        return (s1[i] - s2[i]);
      ++i;
    }
  if ((s1[i] == '\0' && s2[i] == '\0') || i == j)
    return (0);
  if (s1[i] == '\0')
    return (0 - s2[i]);
  else
    return (s1[i]);
}
